/*
*
* Tworzenie grafiki za pomoc� GDI
*
*/
#include <windows.h>
#include <string.h>
#include <stdlib.h>

/* Deklaracja wyprzedzaj�ca: funkcja obs�ugi okna */
LRESULT CALLBACK WindowProcedure(HWND, UINT, WPARAM, LPARAM);
/* Nazwa klasy okna */
char szClassName[] = "Zadanie 1.1.1";

int WINAPI WinMain(HINSTANCE hThisInstance, HINSTANCE hPrevInstance,
	LPSTR lpszArgument, int nFunsterStil)
{
	HWND hwnd;               /* Uchwyt okna */
	MSG messages;            /* Komunikaty okna */
	WNDCLASSEX wincl;        /* Struktura klasy okna */

							 /* Klasa okna */
	wincl.hInstance = hThisInstance;
	wincl.lpszClassName = szClassName;
	wincl.lpfnWndProc = WindowProcedure;    // wska�nik na funkcj� 
											// obs�ugi okna  
	wincl.style = CS_DBLCLKS;
	wincl.cbSize = sizeof(WNDCLASSEX);

	/* Domy�lna ikona i wska�nik myszy */
	wincl.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wincl.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wincl.hCursor = LoadCursor(NULL, IDC_ARROW);
	wincl.lpszMenuName = NULL;
	wincl.cbClsExtra = 0;
	wincl.cbWndExtra = 0;
	/* Jasnoszare t�o */
	wincl.hbrBackground = (HBRUSH)GetStockObject(LTGRAY_BRUSH);

	/* Rejestruj klas� okna */
	if (!RegisterClassEx(&wincl)) return 0;

	/* Tw�rz okno */
	hwnd = CreateWindowEx(
		0,
		szClassName,
		"Zadanie 1.1.1",
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		512,
		512,
		HWND_DESKTOP,
		NULL,
		hThisInstance,
		NULL
	);

	ShowWindow(hwnd, nFunsterStil);
	/* P�tla obs�ugi komunikat�w */
	while (GetMessage(&messages, NULL, 0, 0))
	{
		/* T�umacz kody rozszerzone */
		TranslateMessage(&messages);
		/* Obs�u� komunikat */
		DispatchMessage(&messages);
	}

	/* Zwr�� parametr podany w PostQuitMessage( ) */
	return messages.wParam;
}

int xSize, ySize;

/* T� funkcj� wo�a DispatchMessage( ) */
LRESULT CALLBACK WindowProcedure(HWND hwnd, UINT message,
	WPARAM wParam, LPARAM lParam)
{
	char sText[] = "Witamy w GDI";
	HDC         hdc; // kontekst urz�dzenia
	int         i;
	PAINTSTRUCT ps;
	RECT r;

	HPEN   hPen;
	switch (message)
	{
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	case WM_SIZE:
		xSize = LOWORD(lParam);
		ySize = HIWORD(lParam);

		GetClientRect(hwnd, &r);
		InvalidateRect(hwnd, &r, 1);

		break;
	case WM_PAINT:
		hdc = BeginPaint(hwnd, &ps);

		//osie wykresu
		hPen = CreatePen(PS_SOLID, 1, RGB(0, 0, 0));
		SelectObject(hdc, hPen);

		MoveToEx(hdc, 0, ySize / 2, NULL);
		LineTo(hdc, xSize, ySize/2);

		MoveToEx(hdc, xSize/2, 0, NULL);
		LineTo(hdc, xSize/2, ySize);

		DeleteObject(hPen);

		//f(x) = |x|
		hPen = CreatePen(PS_DASHDOTDOT, 1, RGB(0, 0, 255));
		SelectObject(hdc, hPen);
		
		MoveToEx(hdc, xSize / 2, ySize / 2, NULL);
		LineTo(hdc, xSize / 2 + ySize/2 , 0);

		MoveToEx(hdc, xSize / 2, ySize / 2, NULL);
		LineTo(hdc, xSize / 2 - ySize / 2, 0);

		DeleteObject(hPen);


		//f(x) = x^2
		hPen = CreatePen(PS_DOT, 1, RGB(255, 0, 0));
		SelectObject(hdc, hPen);

		MoveToEx(hdc, xSize / 2, ySize / 2, NULL);
		for (i = 1; i*i < ySize/2; i++)
		{
			LineTo(hdc, xSize / 2 + i, ySize / 2 - i*i);
		}
		LineTo(hdc, xSize / 2 + i, ySize / 2 - i*i);

		MoveToEx(hdc, xSize / 2, ySize / 2, NULL);
		for (i = 1; i*i <= ySize/2; i++)
		{
			LineTo(hdc, xSize / 2 - i, ySize / 2 - i*i);
		}
		LineTo(hdc, xSize / 2 - i, ySize / 2 - i*i);

		DeleteObject(hPen);

		EndPaint(hwnd, &ps);
		break;

	default:
		return DefWindowProc(hwnd, message, wParam, lParam);
	}
	return 0;
}
