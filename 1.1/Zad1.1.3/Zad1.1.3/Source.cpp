/*
*
* Tworzenie okien potomnych
*
*/
#include <windows.h>
#include <string.h>

/* Deklaracja wyprzedzaj�ca: funkcja obs�ugi okna */
LRESULT CALLBACK WindowProcedure(HWND, UINT, WPARAM, LPARAM);
/* Nazwa klasy okna */
char szClassName[] = "Wyb�r uczelni";

#define NUM (sizeof child / sizeof child[0])

int WINAPI WinMain(HINSTANCE hThisInstance, HINSTANCE hPrevInstance,
	LPSTR lpszArgument, int nFunsterStil)
{
	HWND hwnd;               /* Uchwyt okna */
	MSG messages;            /* Komunikaty okna */
	WNDCLASSEX wincl;        /* Struktura klasy okna */

							 /* Klasa okna */
	wincl.hInstance = hThisInstance;
	wincl.lpszClassName = szClassName;
	wincl.lpfnWndProc = WindowProcedure;    // wska�nik na funkcj� 
											// obs�ugi okna  
	wincl.style = CS_DBLCLKS;
	wincl.cbSize = sizeof(WNDCLASSEX);

	/* Domy�lna ikona i wska�nik myszy */
	wincl.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wincl.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wincl.hCursor = LoadCursor(NULL, IDC_ARROW);
	wincl.lpszMenuName = NULL;
	wincl.cbClsExtra = 0;
	wincl.cbWndExtra = 0;
	/* Jasnoszare t�o */
	wincl.hbrBackground = (HBRUSH)GetStockObject(LTGRAY_BRUSH);

	/* Rejestruj klas� okna */
	if (!RegisterClassEx(&wincl)) return 0;

	/* Tw�rz okno */
	hwnd = CreateWindowEx(
		0,
		szClassName,
		"Wyb�r uczelni",
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, CW_USEDEFAULT,
		520, 410,
		HWND_DESKTOP,
		NULL,
		hThisInstance,
		NULL
	);

	ShowWindow(hwnd, nFunsterStil);
	/* P�tla obs�ugi komunikat�w */
	while (GetMessage(&messages, NULL, 0, 0))
	{
		/* T�umacz kody rozszerzone */
		TranslateMessage(&messages);
		/* Obs�u� komunikat */
		DispatchMessage(&messages);
	}

	/* Zwr�� parametr podany w PostQuitMessage( ) */
	return messages.wParam;
}


/* T� funkcj� wo�a DispatchMessage( ) */
LRESULT CALLBACK WindowProcedure(HWND hwnd, UINT message,
	WPARAM wParam, LPARAM lParam)
{
	static int  cxChar, cyChar;
	int         i;

	static HWND groupbox1, groupbox2;
	static HWND editbox1, editbox2;
	static HWND label1, label2, label3;
	static HWND combobox;
	static HWND checkbox1, checkbox2;

	static HWND buttonAccept, buttonCancel;

	switch (message)
	{
	case WM_CREATE:
		cxChar = LOWORD(GetDialogBaseUnits());
		cyChar = HIWORD(GetDialogBaseUnits());

		groupbox1 = CreateWindow("BUTTON",
			"Uczelnia",
			WS_CHILD | WS_VISIBLE |
			BS_GROUPBOX,
			cxChar, cyChar,
			60 * cxChar, 8 * cyChar,
			hwnd, NULL,
			((LPCREATESTRUCT)lParam)->hInstance,
			NULL);

		groupbox2 = CreateWindow("BUTTON",
			"Rodzaj studi�w",
			WS_CHILD | WS_VISIBLE |
			BS_GROUPBOX,
			cxChar, cyChar*10,
			60 * cxChar, 8 * cyChar,
			hwnd, NULL,
			((LPCREATESTRUCT)lParam)->hInstance,
			NULL);

		label1 = CreateWindow("STATIC",
			"Nazwa:",
			WS_CHILD | WS_VISIBLE,
			cxChar, cyChar * 2,
			8 * cxChar, 2 * cyChar,
			groupbox1, NULL,
			((LPCREATESTRUCT)lParam)->hInstance,
			NULL);

		editbox1 = CreateWindow("EDIT",
			"Uniwersytet Wroc�awski",
			WS_CHILD | WS_VISIBLE |
			WS_BORDER,
			10*cxChar, cyChar*2,
			48 * cxChar, 2 * cyChar,
			groupbox1, NULL,
			((LPCREATESTRUCT)lParam)->hInstance,
			NULL);

		label2 = CreateWindow("STATIC",
			"Adres:",
			WS_CHILD | WS_VISIBLE,
			cxChar, cyChar * 5,
			8 * cxChar, 2 * cyChar,
			groupbox1, NULL,
			((LPCREATESTRUCT)lParam)->hInstance,
			NULL);

		editbox2 = CreateWindow("EDIT",
			"pl. Uniwersytecki 1, 50-137 Wroc�aw",
			WS_CHILD | WS_VISIBLE |
			WS_BORDER,
			10*cxChar, cyChar*5,
			48 * cxChar, 2 * cyChar,
			groupbox1, NULL,
			((LPCREATESTRUCT)lParam)->hInstance,
			NULL);

		label3 = CreateWindow("STATIC",
			"Nazwa:",
			WS_CHILD | WS_VISIBLE,
			cxChar, cyChar * 2,
			8 * cxChar, 2 * cyChar,
			groupbox2, NULL,
			((LPCREATESTRUCT)lParam)->hInstance,
			NULL);

		combobox = CreateWindow("COMBOBOX",
			"",
			WS_CHILD | WS_VISIBLE | WS_BORDER | CBS_DROPDOWNLIST,
			10 * cxChar, cyChar*2,
			48 * cxChar, 4 * cyChar,
			groupbox2, NULL,
			((LPCREATESTRUCT)lParam)->hInstance,
			NULL);

		SendMessage(combobox, (UINT)CB_ADDSTRING, (WPARAM)0, (LPARAM)"3 letnie");
		SendMessage(combobox, (UINT)CB_ADDSTRING, (WPARAM)0, (LPARAM)"5 letnie");
		SendMessage(combobox, CB_SETCURSEL, (WPARAM)0, (LPARAM)0);

		checkbox1 = CreateWindow("BUTTON",
			"dzienne",
			WS_CHILD | WS_VISIBLE |
			BS_AUTOCHECKBOX,
			10 * cxChar, cyChar * 5,
			15 * cxChar, 2 * cyChar,
			groupbox2, NULL,
			((LPCREATESTRUCT)lParam)->hInstance,
			NULL);
		
		checkbox2 = CreateWindow("BUTTON",
			"uzupe�niaj�ce",
			WS_CHILD | WS_VISIBLE |
			BS_AUTOCHECKBOX,
			26 * cxChar, cyChar * 5,
			15 * cxChar, 2 * cyChar,
			groupbox2, NULL,
			((LPCREATESTRUCT)lParam)->hInstance,
			NULL);

		buttonAccept = CreateWindow("BUTTON",
			"Akceptuj",
			WS_CHILD | WS_VISIBLE |
			BS_PUSHBUTTON,
			cxChar, cyChar*20,
			10 * cxChar, 2 * cyChar,
			hwnd, (HMENU)IDOK,
			((LPCREATESTRUCT)lParam)->hInstance,
			NULL);

		buttonCancel = CreateWindow("BUTTON",
			"Anuluj",
			WS_CHILD | WS_VISIBLE |
			BS_PUSHBUTTON,
			50*cxChar, cyChar * 20,
			10 * cxChar, 2 * cyChar,
			hwnd, (HMENU)IDCANCEL,
			((LPCREATESTRUCT)lParam)->hInstance,
			NULL);

		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	case WM_COMMAND:
		if (HIWORD(wParam) == BN_CLICKED && LOWORD(wParam) == IDCANCEL)
		{
			PostQuitMessage(0);
		}
		else if (HIWORD(wParam) == BN_CLICKED && LOWORD(wParam) == IDOK)
		{
			TCHAR out[1024] = "";

			TCHAR name[1024];
			GetWindowText(editbox1, name, 1024);
			strcat_s(out, name);
			strcat_s(out, "\n");
			TCHAR adress[1024];
			GetWindowText(editbox2, adress, 1024);
			strcat_s(out, adress);
			strcat_s(out, "\nStudia ");

			int studia = SendMessage(combobox, CB_GETCURSEL, 0, 0);

			if (studia == 0)
			{
				strcat_s(out, "3 letnie\n");
			}
			else if (studia == 1)
			{
				strcat_s(out, "5 letnie\n");
			}
			
			int dzienne = SendMessage(checkbox1, BM_GETCHECK, 0, 0);

			if (dzienne == BST_CHECKED)
			{
				strcat_s(out, "dzienne\n");
			}

			int uzupelniajace = SendMessage(checkbox2, BM_GETCHECK, 0, 0);

			if (uzupelniajace == BST_CHECKED)
			{
				strcat_s(out, "uzupe�niaj�ce\n");
			}

			MessageBox(hwnd, out, TEXT("Uczelnia"), MB_OK);
		}


		break;

	default:
		return DefWindowProc(hwnd, message, wParam, lParam);
	}
	return 0;
}
