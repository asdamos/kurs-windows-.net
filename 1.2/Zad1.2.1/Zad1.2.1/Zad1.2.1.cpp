// Zad1.2.1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>
#include <ShlObj.h>
#include <Windows.h>
#include <string.h>
#include <string>
#include <iostream>

std::string GetLastErrorAsString()
{
	//Get the error message, if any.
	DWORD errorMessageID = ::GetLastError();
	if (errorMessageID == 0)
		return std::string(); //No error message has been recorded

	LPSTR messageBuffer = nullptr;
	size_t size = FormatMessageA(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL, errorMessageID, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPSTR)&messageBuffer, 0, NULL);

	std::string message(messageBuffer, size);

	//Free the buffer.
	LocalFree(messageBuffer);

	return message;
}

int main()
{
	LPWSTR path[1024];
	SHGetFolderPath(NULL, CSIDL_DESKTOPDIRECTORY, NULL, SHGFP_TYPE_CURRENT,(LPWSTR) path);

	printf("%ls\n", path);

	DWORD dwZapisane;
	HANDLE hPlik;

	//nie dziala

	std::wstring pathString((LPWSTR)path);
	std::wstring concatted_stdstr = pathString + L"\\data.txt";
	LPWSTR concatted = (LPWSTR)concatted_stdstr.c_str();

	//strcat_s(path, sizeof("\\data.txt")/sizeof(char), "\\data.txt");
	std::cout << concatted << std::endl;

	SYSTEMTIME time;

	GetSystemTime(&time);
	char bufor[1024] = "";
	sprintf_s(bufor, "Today is %d.%d.%d\n", time.wDay, time.wMonth, time.wYear);

	printf("%s\n", bufor);

	hPlik = CreateFile((LPCWSTR)path, GENERIC_WRITE, 0, NULL, CREATE_NEW, FILE_ATTRIBUTE_NORMAL, NULL);

	if (hPlik == INVALID_HANDLE_VALUE)
	{
		printf("problem\n");
		std::cout << GetLastErrorAsString << std::endl;
	}

	WriteFile(hPlik, bufor, sizeof(bufor) / sizeof(char), &dwZapisane, NULL);

	CloseHandle(hPlik);

	ShellExecute(NULL, (LPCWSTR)"print", (LPCWSTR)path, NULL, NULL, SW_HIDE);

    return 0;
}

