/*
*
* Tworzenie grafiki za pomoc� GDI
*
*/
#include <windows.h>
#include <string.h>
#include <string>

/* Deklaracja wyprzedzaj�ca: funkcja obs�ugi okna */
LRESULT CALLBACK WindowProcedure(HWND, UINT, WPARAM, LPARAM);
/* Nazwa klasy okna */
char szClassName[] = "PRZYKLAD";

int WINAPI WinMain(HINSTANCE hThisInstance, HINSTANCE hPrevInstance,
	LPSTR lpszArgument, int nFunsterStil)
{
	HWND hwnd;               /* Uchwyt okna */
	MSG messages;            /* Komunikaty okna */
	WNDCLASSEX wincl;        /* Struktura klasy okna */

							 /* Klasa okna */
	wincl.hInstance = hThisInstance;
	wincl.lpszClassName = szClassName;
	wincl.lpfnWndProc = WindowProcedure;    // wska�nik na funkcj� 
											// obs�ugi okna  
	wincl.style = CS_DBLCLKS;
	wincl.cbSize = sizeof(WNDCLASSEX);

	/* Domy�lna ikona i wska�nik myszy */
	wincl.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wincl.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wincl.hCursor = LoadCursor(NULL, IDC_ARROW);
	wincl.lpszMenuName = NULL;
	wincl.cbClsExtra = 0;
	wincl.cbWndExtra = 0;
	/* Jasnoszare t�o */
	wincl.hbrBackground = (HBRUSH)GetStockObject(LTGRAY_BRUSH);

	/* Rejestruj klas� okna */
	if (!RegisterClassEx(&wincl)) return 0;

	DWORD resX = 512;
	DWORD resY = 512;

	HKEY hKey;
	LONG res;
	DWORD dwRegDWRD = REG_DWORD;
	DWORD dwBufSize = sizeof(DWORD);
	res = RegOpenKeyEx(HKEY_CURRENT_USER, "Software\\Programowanie pod windows", 0, KEY_ALL_ACCESS, &hKey);
	if (res != ERROR_SUCCESS)
		MessageBox(0, "Error Opening Registry Key", "Error", 0);
	else
	{
		if (RegQueryValueEx(hKey, "ResX", NULL,&dwRegDWRD, (LPBYTE)&resX, &dwBufSize) != ERROR_SUCCESS)
		{
			resX = 512;
			MessageBox(0, "Problem Reading ResX Registry Key", "Error", 0);
		}
		if (RegQueryValueEx(hKey, "ResY", NULL, &dwRegDWRD, (LPBYTE)&resY,  &dwBufSize) != ERROR_SUCCESS)
		{
			resY = 512;
			MessageBox(0, "Problem Reading ResY Registry Key", "Error", 0);
		}
	}
	RegCloseKey(hKey);

	/* Tw�rz okno */
	hwnd = CreateWindowEx(
		0,
		szClassName,
		"PRZYKLAD",
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		resX,
		resY,
		HWND_DESKTOP,
		NULL,
		hThisInstance,
		NULL
	);

	ShowWindow(hwnd, nFunsterStil);
	/* P�tla obs�ugi komunikat�w */
	while (GetMessage(&messages, NULL, 0, 0))
	{
		/* T�umacz kody rozszerzone */
		TranslateMessage(&messages);
		/* Obs�u� komunikat */
		DispatchMessage(&messages);
	}

	/* Zwr�� parametr podany w PostQuitMessage( ) */
	return messages.wParam;
}

int xSize, ySize;

/* T� funkcj� wo�a DispatchMessage( ) */
LRESULT CALLBACK WindowProcedure(HWND hwnd, UINT message,
	WPARAM wParam, LPARAM lParam)
{
	RECT r;


	switch (message)
	{
	case WM_DESTROY:

		HKEY hKey;
		LONG res;
		HKEY hkeyresult;
		GetWindowRect(hwnd, &r);
		DWORD tmpx, tmpy;
		tmpx = r.right - r.left;
		tmpy = r.bottom - r.top;
		res = RegOpenKeyEx(HKEY_CURRENT_USER, "Software\\Programowanie pod windows", 0, KEY_ALL_ACCESS, &hKey);
		if (res != ERROR_SUCCESS)
		{
			MessageBox(0, "Error Opening Registry Key", "Error", 0);

			res = RegCreateKey(HKEY_CURRENT_USER, "Software\\Programowanie pod windows", &hkeyresult);

			if (res != ERROR_SUCCESS)
			{
				MessageBox(0, "Error Creating Registry Key", "Error", 0);
			}
			else
			{
				if (RegSetValueEx(hkeyresult, "ResX", 0, REG_DWORD, (const BYTE*)&tmpx, sizeof(DWORD)) != ERROR_SUCCESS)
				{
					MessageBox(0, "Error Writing to  Registry Key", "Error", 0);
				}
				if (RegSetValueEx(hkeyresult, "ResY", 0, REG_DWORD, (const BYTE*)&tmpy, sizeof(DWORD)) != ERROR_SUCCESS)
				{
					MessageBox(0, "Error Writing to  Registry Key", "Error", 0);
				}
			}
			RegCloseKey(hkeyresult);
		}
		else
		{
			if (RegSetValueEx(hKey, "ResX", 0, REG_DWORD, (const BYTE*)&tmpx, sizeof(DWORD)) != ERROR_SUCCESS)
			{
				MessageBox(0, "Error Writing to  Registry Key", "Error", 0);
			}

			if (RegSetValueEx(hKey, "ResY", 0, REG_DWORD, (const BYTE*)&tmpy, sizeof(DWORD)) != ERROR_SUCCESS)
			{
				MessageBox(0, "Error Writing to  Registry Key", "Error", 0);
			}

		}
		RegCloseKey(hKey);

		PostQuitMessage(0);
		break;
	case WM_SIZE:
		xSize = LOWORD(lParam);
		ySize = HIWORD(lParam);

		GetClientRect(hwnd, &r);
		InvalidateRect(hwnd, &r, 1);

		

		break;

	default:
		return DefWindowProc(hwnd, message, wParam, lParam);
	}
	return 0;
}
