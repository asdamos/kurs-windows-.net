#include <Windows.h>
#include <cstdio>
#include <ctime>
#include <cstdlib>
#include <iostream>

void watekDostawcy() {
	HANDLE sDostawca = OpenSemaphore(SEMAPHORE_ALL_ACCESS, false, L"SEM_DOSTAWCA");
	HANDLE sTyton = OpenSemaphore(SEMAPHORE_ALL_ACCESS, false, L"SEM_TYTON");
	HANDLE sPapier = OpenSemaphore(SEMAPHORE_ALL_ACCESS, false, L"SEM_PAPIER");
	HANDLE sZapalki = OpenSemaphore(SEMAPHORE_ALL_ACCESS, false, L"SEM_ZAPALKI");
	HANDLE sGlowny = OpenSemaphore(SEMAPHORE_ALL_ACCESS, false, L"SEM_GLOWNY");

	while (true) {
		WaitForSingleObject(sGlowny, INFINITE);
		int i = rand() % 3;
		switch (i) {
		case 0:
			std::cout << "Dostawca dostarcza papier i zapalki"<<std::endl;
			ReleaseSemaphore(sTyton, 1, NULL);
			break;
		case 1:
			std::cout << "Dostawca dostarcza tyton i zapalki" << std::endl;
			ReleaseSemaphore(sPapier, 1, NULL);
			break;
		case 2:
			std::cout << "Dostawca dostarcza papier i tyton" << std::endl;
			ReleaseSemaphore(sZapalki, 1, NULL);
			break;
		}
		ReleaseSemaphore(sGlowny, 1, NULL);
		WaitForSingleObject(sDostawca, INFINITE);
	}
}

void watekPalacza(LPVOID *arg) {
	
	//acces, inherit handle, name
	HANDLE sDostawca = OpenSemaphore(SEMAPHORE_ALL_ACCESS, false, L"SEM_DOSTAWCA");
	HANDLE sTyton = OpenSemaphore(SEMAPHORE_ALL_ACCESS, false, L"SEM_TYTON");
	HANDLE sPapier = OpenSemaphore(SEMAPHORE_ALL_ACCESS, false, L"SEM_PAPIER");
	HANDLE sZapalki = OpenSemaphore(SEMAPHORE_ALL_ACCESS, false, L"SEM_ZAPALKI");
	HANDLE sGlowny = OpenSemaphore(SEMAPHORE_ALL_ACCESS, false, L"SEM_GLOWNY");

	int i = (int)arg;
	while(true) {
		switch (i) {

		case 0:
			WaitForSingleObject(sTyton, INFINITE);
			WaitForSingleObject(sGlowny, INFINITE);

			std::cout << "Palacz z tytoniem" << std::endl;

			ReleaseSemaphore(sDostawca, 1, NULL);
			ReleaseSemaphore(sGlowny, 1, NULL);
			break;
		case 1:
			WaitForSingleObject(sPapier, INFINITE);
			WaitForSingleObject(sGlowny, INFINITE);

			std::cout << "Palacz z papierem" << std::endl;

			ReleaseSemaphore(sDostawca, 1, NULL);
			ReleaseSemaphore(sGlowny, 1, NULL);
			break;
		case 2:
			WaitForSingleObject(sZapalki, INFINITE);
			WaitForSingleObject(sGlowny, INFINITE);

			std::cout << "Palacz z zapalkami" << std::endl;

			ReleaseSemaphore(sDostawca, 1, NULL);
			ReleaseSemaphore(sGlowny, 1, NULL);
			break;
		}
	}
}


int main() {
	srand((unsigned)time(NULL));
	HANDLE dostawca, palacze[3];
	//security attributes, initial count, max count, name
	HANDLE sDostawca = CreateSemaphore(NULL, 0, 1, L"SEM_DOSTAWCA");
	HANDLE sTyton = CreateSemaphore(NULL, 0, 1, L"SEM_TYTON");
	HANDLE sPapier = CreateSemaphore(NULL, 0, 1, L"SEM_PAPIER");
	HANDLE sZapalki = CreateSemaphore(NULL, 0, 1, L"SEM_ZAPALKI");
	HANDLE sGlowny = CreateSemaphore(NULL, 1, 1, L"SEM_GLOWNY");


	for (int i = 0; i < 3; i++) {
		palacze[i] = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)watekPalacza, (LPVOID)i, NULL, NULL);
	}

	dostawca = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)watekDostawcy, NULL, NULL, NULL);
	
	//nigdy sie nei skonczy
	WaitForMultipleObjects(3, palacze, true, INFINITE);

	//sekunda dla wszystkich
	//WaitForMultipleObjects(3, palacze, true, 1000);

	for (int i = 0; i < 3; i++) {
		TerminateThread(palacze[i], 0);
		CloseHandle(palacze[i]);
	}
	TerminateThread(dostawca, 0);
	CloseHandle(dostawca);
	return 0;
}