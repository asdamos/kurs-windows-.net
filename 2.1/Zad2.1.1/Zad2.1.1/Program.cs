﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad2._1._1
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int i = 1; i <= 100000; i++)
            {
                int tmp = i;
                int suma = 0;
                while (tmp > 0)
                {
                    int cyfra = tmp % 10;
                    if (cyfra == 0)
                    {
                        break;
                    }
                    else
                    {
                        if ((i % cyfra) != 0)
                        {
                            break;
                        }
                    }
                    
                    suma += cyfra;
                    tmp /= 10;
                }
                if (tmp == 0 && i % suma == 0)
                {
                    Console.WriteLine(i);
                }
                    
            }

            Console.ReadKey();
        }
    }
}
