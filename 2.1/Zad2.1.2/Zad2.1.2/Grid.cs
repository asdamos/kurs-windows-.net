﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad2._1._2
{
    class Grid
    {
        private int[][] array;
        private int x;
        private int y;

        public Grid(int x, int y)
        {
            this.x = x;
            this.y = y;
            array = new int[x][];
            for(int i=0; i<x; i++)
            {
                array[i] = new int[y];
            }
        }

        public int[] this[int x]
        {
            get
            {
                return array[x];
            }

            set
            {
                array[x] = value;
            }
        }

        public int this[int x, int y]
        {
            get
            {
                return array[x][y];
            }
            set
            {
                array[x][y] = value;
            }
        }

    }
}
