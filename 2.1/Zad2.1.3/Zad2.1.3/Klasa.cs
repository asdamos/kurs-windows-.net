﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad2._1._3
{
    class Klasa
    {
        public Klasa()
        {
            polePubliczne = 1;
            polePrywatne = 1;
        }

        private int polePrywatne;

        //właściwość / propercja

        private int pPrywatne
        {
            get
            {
                polePrywatne *= 2;
                return polePrywatne;
            }
        }

        private int metodaPrywatna()
        {
            return 100;
        }

        private int mPrywatna
        {
            get
            {
                return metodaPrywatna();
            }
        }


        public int polePubliczne;

        public int pPubliczne
        {
            get
            {
                polePubliczne += 2;
                return polePubliczne;
            }
        }



    }
}
