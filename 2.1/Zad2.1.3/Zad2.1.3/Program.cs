﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Zad2._1._3
{
    class Program
    {
        static void Main(string[] args)
        {
            Klasa ob = new Klasa();

            var prywatnePola = ob.GetType().GetFields(BindingFlags.Instance | BindingFlags.NonPublic);
            var prywatnePropercje = ob.GetType().GetProperties(BindingFlags.Instance | BindingFlags.NonPublic);

            foreach (var p in prywatnePropercje)
            {
                Console.WriteLine("Propercja {0}, wartość: {1}", p.Name,p.GetValue(ob));
            }

            foreach (var p in prywatnePola)
            {
                Console.WriteLine("Pole {0}, wartość: {1}", p.Name, p.GetValue(ob));
            }

            Console.WriteLine("\nPorównanie szybkości dostępu do propercji publicznej w sposób zwykły oraz za pomocą refleksji\n");

            int tmp = 0;
            DateTime End;
            DateTime Start = DateTime.Now;

            for (int i = 0; i < 10000000; i++)
            {
                tmp += ob.pPubliczne;
            }
            End = DateTime.Now;
            Console.WriteLine("Dostęp bezpośredni do propercji publicznej: {0}. Wynik {1}", (End - Start), tmp);

            ob.polePubliczne = 1;
            tmp = 0;
            Start = DateTime.Now;
            var propercja = ob.GetType().GetProperty("pPubliczne");
            for (int i = 0; i < 10000000; i++)
            {
                tmp += (int)propercja.GetValue(ob);
            }
            End = DateTime.Now;
            Console.WriteLine("Dostęp za pomocą refleksji do propercji publicznej: {0}. Wynik {1}", (End - Start), tmp);


        }
    }
}
