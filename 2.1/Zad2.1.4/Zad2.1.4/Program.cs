﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Zad2._1._4
{
    public class Oznakowane : Attribute { }
    public class Foo
    {
        [Oznakowane]
        public int Bar()
        {
            return 1;
        }
        public int Qux()
        {
            return 2;
        }

        [Oznakowane]
        public int Metoda1()
        {
            return 3;
        }

        [Oznakowane]
        public int Metoda2(int x)
        {
            return 3 + x;
        }

        [Oznakowane]
        public string Metoda3()
        {
            return "aaa";
        }

    }
   


    class Program
    {
        static void wypiszOznakowane(object x)
        {
            Type type = x.GetType();
            MethodInfo[] metodyKlasy = type.GetMethods(BindingFlags.Public | BindingFlags.Instance);

            int oznakowaneIt = 0;
            MethodInfo[] metodyIntOznakowane = new MethodInfo[metodyKlasy.Length];

            for(int i=0; i<metodyKlasy.Length; i++)
            {
                if(metodyKlasy[i].ReturnType == typeof(int) && 
                    metodyKlasy[i].GetParameters().Length == 0 &&
                    metodyKlasy[i].GetCustomAttributes(typeof(Oznakowane)).Count() == 1)
                {
                    metodyIntOznakowane[oznakowaneIt] = metodyKlasy[i];
                    oznakowaneIt++;
                }

            }
            for (int i = 0; i < oznakowaneIt; i++)
            {
                Console.WriteLine("Metoda: {0}: {1}", metodyIntOznakowane[i], metodyIntOznakowane[i].Invoke(x, null));
            }
        }


        static void Main(string[] args)
        {
            Foo ob = new Foo();
            wypiszOznakowane(ob);
        }
    }
}
