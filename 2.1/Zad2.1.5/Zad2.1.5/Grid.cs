﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad2._1._5
{
    /// <summary>
    /// Klasa Grid
    /// </summary>
    class Grid
    {
        private int[][] array;
        private int x;
        private int y;

        /// <summary>
        /// Konstruktor siatki
        /// </summary>
        /// <param name="x">Liczba wierszy</param>
        /// <param name="y">Liczba kolumn</param>
        public Grid(int x, int y)
        {
            this.x = x;
            this.y = y;
            array = new int[x][];
            for (int i = 0; i < x; i++)
            {
                array[i] = new int[y];
            }
        }

        /// <summary>
        /// Indekser jednowymiarowy
        /// </summary>
        /// <param name="x">Indeks wiersza</param>
        /// <returns>Wiersz siatki pod indeksem x</returns>
        public int[] this[int x]
        {
            get
            {
                return array[x];
            }

            set
            {
                array[x] = value;
            }
        }

        /// <summary>
        /// Indekser dwuargumentowy
        /// </summary>
        /// <param name="x">Nr wiersza komórki</param>
        /// <param name="y">Nr kolumny komórki</param>
        /// <returns>Wartość komórki</returns>
        public int this[int x, int y]
        {
            get
            {
                return array[x][y];
            }
            set
            {
                array[x][y] = value;
            }
        }

    }
}
