﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad2._1._5
{
    class Program
    {
        static void Main(string[] args)
        {
            Grid grid = new Grid(4, 4);

            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    grid[i, j] = i + j;
                }
            }

            for (int i = 0; i < 4; i++)
            {
                int[] rowdata = grid[i];
                for (int j = 0; j < rowdata.Length; j++)
                {
                    Console.Write("{0} ", rowdata[j]);
                }
                Console.WriteLine();
            }


            grid[0, 0] = 100;

            Console.WriteLine("{0}", grid[0, 0]);

        }
    }
}
