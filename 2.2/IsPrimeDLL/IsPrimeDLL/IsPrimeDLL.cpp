// IsPrimeDLL.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"

extern "C" __declspec(dllexport) bool isPrime(int num)
{
	if (num <= 1) return 0;
	if (num % 2 == 0 && num > 2) return 0;
	for (int i = 3; i < num / 2; i += 2)
	{
		if (num % i == 0)
			return 0;
	}
	return 1;
}

extern "C" 	__declspec(dllexport) int _stdcall executeC(int n, int(_stdcall *f)(int)) 
{
		return f(n);
}