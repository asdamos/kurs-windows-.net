﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace Zad2._2._1
{
    class Program
    {
        [DllImport("Advapi32.dll")]
        public static extern bool GetUserName(StringBuilder lpBuffer, ref int nSize);


        [DllImport("user32.dll",EntryPoint = "MessageBox")]
        public static extern int MsgBox(int hWnd, [MarshalAs(UnmanagedType.LPStr)] String text, String caption, uint type);

        static void Main(string[] args)
        {
            StringBuilder Buffer = new StringBuilder(64);
            int nSize = 64;
            GetUserName(Buffer, ref nSize);


            MsgBox(0, Buffer.ToString(), "", 0);
        }
    }
}
