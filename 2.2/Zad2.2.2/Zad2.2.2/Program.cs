﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace Zad2._2._2
{
    class Program
    {
        [DllImport(@"C:\Users\Adam\repos\kurs-windows-.net\2.2\IsPrimeDLL\Debug\IsPrimeDLL.dll")]
        public static extern bool isPrime(int num);

        static void Main(string[] args)
        {
            int num;
            num = Int32.Parse(Console.ReadLine());

            if (isPrime(num))
                Console.WriteLine("Pierwsza");
            else
                Console.WriteLine("Nie jest pierwsza");

        }
    }
}
