﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Zad2._2._3
{
    class Program
    {
        public static int IsPrimeCs(int num)
        {
            if (num <= 1) return 0;
            if (num % 2 == 0 && num > 2) return 0;
            for (int i = 3; i < num / 2; i += 2)
            {
                if (num % i == 0)
                    return 0;
            }
            return 1;
        }

        public delegate int Delegata(int i);

        [DllImport(@"C:\Users\Adam\repos\kurs-windows-.net\2.2\IsPrimeDLL\Debug\IsPrimeDLL.dll")]
        public static extern int executeC(int x, Delegata f);

        static void Main(string[] args)
        {
            int num;
            num = Int32.Parse(Console.ReadLine());

            Delegata d = IsPrimeCs;

            if (executeC(num, d)==1)
                Console.WriteLine("Pierwsza");
            else
                Console.WriteLine("Nie jest pierwsza");

        }
    }
}
