﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Zad2._3._1
{
    class Program
    {
        static void listaTesty()
        {
            ArrayList tabList = new ArrayList();
            List<int> lista = new List<int>();

            //Dodawanie elementow

            DateTime poczatek, koniec;

            poczatek = DateTime.Now;

            for(int i=0; i<10000000; i++)
            {
                tabList.Add(i);
            }
            koniec = DateTime.Now;

            Console.WriteLine("Dodawanie do ArrayList: {0}", koniec - poczatek);

            poczatek = DateTime.Now;

            for (int i = 0; i < 10000000; i++)
            {
                lista.Add(i);
            }
            koniec = DateTime.Now;

            Console.WriteLine("Dodawanie do List<int>: {0}", koniec - poczatek);

            //Przegladanie
            int sum = 0;

            poczatek = DateTime.Now;

            for (int i = 0; i < 10000000; i++)
            {
                sum += (int)tabList[i];
            }
            koniec = DateTime.Now;

            Console.WriteLine("Przeglądanie ArrayList: {0}", koniec - poczatek);

            sum = 0;
            poczatek = DateTime.Now;

            for (int i = 0; i < 10000000; i++)
            {
                sum += lista[i];
            }
            koniec = DateTime.Now;

            Console.WriteLine("Przeglądanie List<int>: {0}", koniec - poczatek);

            //Usuwanie
            poczatek = DateTime.Now;

            for (int i = 0; i < 100; i++)
            {
                tabList.RemoveAt(0);
            }
            koniec = DateTime.Now;

            Console.WriteLine("Usuwanie ArrayList: {0}", koniec - poczatek);

            poczatek = DateTime.Now;

            for (int i = 0; i < 100; i++)
            {
                lista.RemoveAt(0);
            }
            koniec = DateTime.Now;

            Console.WriteLine("Usuwanie List<int>: {0}", koniec - poczatek);
        }

        static void slownikiTesty()
        {
            Hashtable hashtable = new Hashtable();
            Dictionary<string, int> dict = new Dictionary<string, int>();

            //Dodawanie elementow

            DateTime poczatek, koniec;

            poczatek = DateTime.Now;

            for (int i = 0; i < 1000000; i++)
            {
                hashtable.Add(i.ToString(), i);
            }
            koniec = DateTime.Now;

            Console.WriteLine("Dodawanie do Hashtable: {0}", koniec - poczatek);

            poczatek = DateTime.Now;

            for (int i = 0; i < 1000000; i++)
            {
                dict.Add(i.ToString(), i);
            }
            koniec = DateTime.Now;

            Console.WriteLine("Dodawanie do Dictionary<string, int>: {0}", koniec - poczatek);

            //Przegladanie
            int sum = 0;

            poczatek = DateTime.Now;

            for (int i = 0; i < 1000000; i++)
            {
                sum += (int)hashtable[i.ToString()];
            }
            koniec = DateTime.Now;

            Console.WriteLine("Przeglądanie Hashtable: {0}", koniec - poczatek);

            sum = 0;
            poczatek = DateTime.Now;

            for (int i = 0; i < 1000000; i++)
            {
                sum += dict[i.ToString()];
            }
            koniec = DateTime.Now;

            Console.WriteLine("Przeglądanie Dictionary<string, int>: {0}", koniec - poczatek);

            //Usuwanie
            poczatek = DateTime.Now;

            for (int i = 0; i < 1000000; i++)
            {
                hashtable.Remove(i.ToString());
            }
            koniec = DateTime.Now;

            Console.WriteLine("Usuwanie Hashtable: {0}", koniec - poczatek);

            poczatek = DateTime.Now;

            for (int i = 0; i < 1000000; i++)
            {
                dict.Remove(i.ToString());
            }
            koniec = DateTime.Now;

            Console.WriteLine("Usuwanie Dictionary<string, int>: {0}", koniec - poczatek);
        }

        static void Main(string[] args)
        {
            listaTesty();

            Console.WriteLine();

            slownikiTesty();


        }
    }
}
