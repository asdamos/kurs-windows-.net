﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad2._3._2
{
    class Program
    {
        static void Main(string[] args)
        {
            BinaryTreeNode<int> tree = new BinaryTreeNode<int>()
            {
                Val = 5,
                Left = new BinaryTreeNode<int>()
                {
                    Val = 1,
                    Left = new BinaryTreeNode<int>()
                    {
                        Val = -5
                    },
                    Right = new BinaryTreeNode<int>()
                    {
                        Val = 3
                    }
                },
                Right = new BinaryTreeNode<int>()
                {
                    Val = 10,
                    Left = new BinaryTreeNode<int>()
                    {
                        Val = 7
                    },
                    Right = new BinaryTreeNode<int>()
                    {
                        Val = 15
                    }
                }
            };

            Console.WriteLine("DFS");
            foreach(var v in tree)
            {
                Console.WriteLine(v);
            }

            Console.WriteLine("\nBFS");
            var BFSEnumerator = tree.GetEnumeratorBFSBezYield();
            while (BFSEnumerator.MoveNext())
            {
                Console.WriteLine(BFSEnumerator.Current);
            }
        }
    }

    class BinaryTreeNode<T> : IEnumerable<T>
    {
        public T Val;
        public BinaryTreeNode<T> Left = null;
        public BinaryTreeNode<T> Right = null;


        public IEnumerator<T> GetEnumerator()
        {
            yield return Val;
            if (Left != null)
            {
                foreach (T v in Left)
                {
                    yield return v;
                }
            }
            if (Right != null)
            {
                foreach (T v in Right)
                {
                    yield return v;
                }
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        //---------------------------------------------

        

        public IEnumerator<T> GetEnumeratorBFS()
        {
            Queue<BinaryTreeNode<T>> q = new Queue<BinaryTreeNode<T>>();
            q.Enqueue(this);    //dodawanie elementu do kolejki
            while (q.Count > 0)
            {
                BinaryTreeNode<T> tmp = q.Dequeue();    //wyciaganie elementu z kolejki
                if (tmp.Left != null) q.Enqueue(tmp.Left);
                if (tmp.Right != null) q.Enqueue(tmp.Right);
                yield return tmp.Val;
            }
        }

        public IEnumerator<T> GetEnumeratorBFSBezYield()
        {
            return new TreeEnumeratorBFS<T>(this);
        }

        public IEnumerator<T> GetEnumeratorDFSBezYield()
        {
            return new TreeEnumeratorDFS<T>(this);
        }

    }

    class TreeEnumeratorDFS<T> : IEnumerator<T>
    {
        BinaryTreeNode<T> current = null;
        Stack<BinaryTreeNode<T>> stack = new Stack<BinaryTreeNode<T>>();

        public TreeEnumeratorDFS(BinaryTreeNode<T> tree)
        {
            stack.Push(tree);
        }

        object IEnumerator.Current => current;

        T IEnumerator<T>.Current
        {
            get
            {
                return current.Val;
            }
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public bool MoveNext()
        {
            if (stack.Count == 0) return false;
            current = stack.Pop();
            if (current.Left != null) stack.Push(current.Left);
            if (current.Right != null) stack.Push(current.Right);
            return true;
        }

        public void Reset()
        {
            throw new NotImplementedException();
        }
    }

    class TreeEnumeratorBFS<T> : IEnumerator<T>
    {
        BinaryTreeNode<T> current = null;
        Queue<BinaryTreeNode<T>> queue = new Queue<BinaryTreeNode<T>>();

        public TreeEnumeratorBFS(BinaryTreeNode<T> tree)
        {
            queue.Enqueue(tree);
        }

        public T Current
        {
            get
            {
                return current.Val;
            }
        }

        object IEnumerator.Current => current;

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public bool MoveNext()
        {
            if (queue.Count == 0) return false;
            current = queue.Dequeue();
            if (current.Left != null) queue.Enqueue(current.Left);
            if (current.Right != null) queue.Enqueue(current.Right);
            return true;
        }

        public void Reset()
        {
            throw new NotImplementedException();
        }
    }
}

