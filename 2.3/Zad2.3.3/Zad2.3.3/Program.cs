﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Zad2._3._3
{
    public delegate TOutput Delegata1<in TInput, out TOutput>(TInput input);


    class Program
    {
        public static string fun1(int i)
        {
            return i.ToString();
        }

        static void Main(string[] args)
        {
            List<int> lista = new List<int>();

            for(int i=0; i<5; i++)
            {
                lista.Add(i);
            }

            Delegata1<int, string> d1= delegate (int i) { return (i + 5).ToString(); };

            //List<string> listaStr = lista.ConvertAll<string>(new Delegata1<int, string>(fun1));
            List<string> listaStr = lista.ConvertAll<string>(new System.Converter<int, string>(fun1));

            foreach (var v in listaStr)
            {
                Console.WriteLine(v);
            }

            Console.WriteLine();
            List<int> parzyste = lista.FindAll(delegate (int i) { return (i % 2 == 0); });
            foreach (var v in parzyste)
            {
                Console.WriteLine(v);
            }

            Console.WriteLine();
            lista.ForEach(delegate (int i) { Console.WriteLine(i * i); });


            lista.RemoveAll(delegate (int i) { return (i % 2 == 1); });
            Console.WriteLine();
            foreach (var v in lista)
            {
                Console.WriteLine(v);
            }

            List<int> list = new List<int>();
            list.Add(5);
            list.Add(1);
            list.Add(100);
            list.Add(-5);
            list.Add(11);
            list.Add(0);

            Comparison<int> c = delegate (int x, int y)
            {
                if (y > x)
                    return 1;
                else if (y == x)
                    return 0;
                else return -1;
            };
            list.Sort(c);

            Console.WriteLine();
            foreach (var v in list)
            {
                Console.WriteLine(v);
            }

        }
    }
}
