﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad2._3._4
{
    class Program
    {
        public static string fun1(int i)
        {
            return i.ToString();
        }

        static void Main(string[] args)
        {
            List<int> lista = new List<int>();

            for (int i = 0; i < 5; i++)
            {
                lista.Add(i);
            }
            List<string> listaStr = ListHelper.ConvertAll(lista,new System.Converter<int, string>(fun1));

            foreach (var v in listaStr)
            {
                Console.WriteLine(v);
            }

            Console.WriteLine();
            List<int> parzyste = ListHelper.FindAll(lista,delegate (int i) { return (i % 2 == 0); });
            foreach (var v in parzyste)
            {
                Console.WriteLine(v);
            }

            Console.WriteLine();
            ListHelper.ForEach(lista,delegate (int i) { Console.WriteLine(i * i); });


            ListHelper.RemoveAll(lista, delegate (int i) { return (i % 2 == 1); });
            Console.WriteLine();
            foreach (var v in lista)
            {
                Console.WriteLine(v);
            }

            List<int> list = new List<int>();
            list.Add(5);
            list.Add(1);
            list.Add(100);
            list.Add(-5);
            list.Add(11);
            list.Add(0);

            Comparison<int> c = delegate (int x, int y)
            {
                if (y > x)
                    return 1;
                else if (y == x)
                    return 0;
                else return -1;
            };
            ListHelper.Sort(list, c);

            Console.WriteLine();
            foreach (var v in list)
            {
                Console.WriteLine(v);
            }
        }
    }

    public class ListHelper
    {
        public static List<TOutput> ConvertAll<T, TOutput>(List<T> list,Converter<T, TOutput> converter)
        {
            List<TOutput> res = new List<TOutput>();
            foreach(T e in list)
            {
                res.Add(converter(e));
            }
            return res;
        }
        public static List<T> FindAll<T>(List<T> list,Predicate<T> match)
        {
            List<T> res = new List<T>();
            foreach (T e in list)
            {
                if(match(e))
                {
                    res.Add(e);
                }
            }
            return res;
        }
        public static void ForEach<T>(List<T> list, Action<T> action)
        {
            foreach (T e in list)
            {
                action(e);
            }
        }
        public static int RemoveAll<T>(List<T> list,Predicate<T> match)
        {
            int i = 0, res = 0;
            while (i < list.Count)
            {
                if (match(list[i]))
                {
                    list.RemoveAt(i);
                    res++;
                }
                else i++;
            }
            return res;
        }
        public static void Sort<T>(List<T> list, Comparison<T> comparison)
        {
            for (int i = 0; i < list.Count; i++)
            {
                for (int j = i; j < list.Count - 1; j++)
                {
                    if (comparison(list[j], list[j + 1]) > 0)
                    {
                        T tmp = list[j];
                        list[j] = list[j + 1];
                        list[j + 1] = tmp;
                    }
                }
            }
        }
    }
}
