﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad2._4._1
{
    class Program
    {
        static void Main(string[] args)
        {

            string s = "Kobyła ma mały bok";
            bool ispalindrome = s.IsPalindrome();
            Console.WriteLine(ispalindrome);
        }
    }

    static class StringHelper
    {
        public static bool IsPalindrome(this string str)
        {
            str = str.ToLower();

            string str2 = "";

            for(int i=0; i<str.Length; i++)
            {
                char c = str[i];

                if(!char.IsWhiteSpace(c))
                {
                    str2 = str2 + c;
                }
            }
            for(int i=0; i<str2.Length/2; i++)
            {
                if(str2[i] != str2[str2.Length-1-i])
                {
                    return false;
                }
            }

            return true;
        }
    }

}
