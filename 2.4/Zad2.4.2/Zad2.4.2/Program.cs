﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Zad2._4._2
{
    class Program
    {
        static void Main(string[] args)
        {
            var liczby_string = File.ReadAllLines("liczby.txt");
            List<int> liczby = new List<int>();

            foreach (var s in liczby_string)
            {
                int tmp;
                if (int.TryParse(s, out tmp))
                {
                    liczby.Add(tmp);
                }

            }

            var a = from l in liczby
                    where l > 100
                    orderby l descending
                    select l;

            var b = liczby.Where(i => i > 100).OrderByDescending(i => i);

            Console.WriteLine("Wyrażenie LINQ:");
            foreach (int l in a) Console.Write("{0}, ", l);

            Console.WriteLine();
            Console.WriteLine("Ciąg wywołań LINQ:");
            foreach (int l in b) Console.Write("{0}, ", l);

            Console.WriteLine();


        }
    }
}
