﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Zad2._4._3
{
    class Program
    {
        static void Main(string[] args)
        {
            var nazwiska = File.ReadAllLines("nazwiska.txt");

            var litery = from x in nazwiska
                         group x by x[0] into a
                         orderby a.Key
                         select a.Key;

            Console.Write("(");
            foreach (char c in litery) Console.Write("{0}, ", c);
            Console.WriteLine(")");


        }
    }
}
