﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Zad2._4._4
{
    class Program
    {
        static void Main(string[] args)
        {
            var folder = new DirectoryInfo(Directory.GetCurrentDirectory());
            var suma = (from file in folder.EnumerateFiles()
                        select file.Length).Aggregate((x, y) => x + y);

            Console.WriteLine("Suma długości plików w tym katalogu to {0}", suma);
        }
    }
}
