﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;


namespace Zad2._4._5
{
    class Program
    {
        static void Main(string[] args)
        {
            var klienci = File.ReadAllLines("pierwszy.txt");

            var konta = File.ReadAllLines("drugi.txt");

            List<Klient> Klienci = new List<Klient>();
            List<Konto> Konta = new List<Konto>();

            foreach(var s in klienci)
            {
                var tmp = s.Split(',');
                Klienci.Add(new Klient(tmp[0], tmp[1], tmp[2]));
            }

            foreach (var s in konta)
            {
                var tmp = s.Split(',');
                Konta.Add(new Konto(tmp[0], tmp[1]));
            }

            
            var dane = from a in Klienci
                       join b in Konta on a.PESEL equals b.PESEL
                       select new
                       {
                           Imie = a.Imie,
                           Nazwisko = a.Nazwisko,
                           PESEL = a.PESEL,
                           NrKonta = b.NrKonta
                       };

            foreach (var d in dane)
            {
                Console.WriteLine("{0}, {1}, {2}, {3}", d.Imie, d.Nazwisko, d.PESEL, d.NrKonta);
            }

        }
    }

    class Klient
    {
        public string Imie;
        public string Nazwisko;
        public string PESEL;

        public Klient(string Imie, string Nazwisko, string Pesel)
        {
            this.Imie = Imie;
            this.Nazwisko = Nazwisko;
            this.PESEL = Pesel;
        }
        
    }

    class Konto
    {
        public string NrKonta;
        public string PESEL;

        public Konto(string Pesel, string NrKonta)
        {
            this.NrKonta = NrKonta;
            this.PESEL = Pesel;
        }

    }

}
