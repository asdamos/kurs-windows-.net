﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Zad2._4._6
{
    class Program
    {
        static void Main(string[] args)
        {
            var log = File.ReadAllLines("log.txt");

            List<LogEntry> Log = new List<LogEntry>();
            foreach (var s in log)
            {
                var tmp = s.Split(' ');
                Log.Add(new LogEntry(tmp[0], tmp[1], tmp[2], tmp[3]));
            }



            var res = (from a in Log
                       group a by a.IP into b
                       orderby b.Count() descending
                       select new { IP = b.Key, Count = b.Count() }).Take(3);

            foreach (var a in res)
            {
                Console.WriteLine("{0} wystepje {1} razy", a.IP, a.Count);
            }
        }
    }

    class LogEntry
    {
        public string Hour;
        public string IP;
        public string Resource;
        public string Code;

        public LogEntry(string Hour, string IP, string Resource, string Code)
        {
            this.Hour = Hour;
            this.IP = IP;
            this.Resource = Resource;
            this.Code = Code;
        }

    }
}
