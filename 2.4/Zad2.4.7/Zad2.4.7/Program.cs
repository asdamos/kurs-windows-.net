﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//nalezy wykorzystac mechanizm typow dynamicznych
namespace Zad2._4._7
{
    class Program
    {
        static void Main(string[] args)
        {
            var item = new { Field1 = "The value", Field2 = 5 };

            var List = new List<dynamic>();

            List.Add(item);
            List.Add(new { Field1 = "Other value", Field2 = 100 });
            
            //List.Add(new { Field3 = "Other type", Field4 = 100 });

            foreach (var v in List)
            {
                Console.WriteLine("{0} {1}", v.Field1, v.Field2);
            }


        }
    }
}
