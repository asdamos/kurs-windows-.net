﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad2._5._1
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = 10000000;

            int tmp;
            object tmp2;

            DateTime Start, End;

            Start = DateTime.Now;
            for(int i=0; i<n; ++i)
            {
                tmp = Foo(i, n - i);

            }
            End = DateTime.Now;

            Console.WriteLine("Normalna funkcja: {0}", End - Start);

            Start = DateTime.Now;
            for (int i = 0; i < n; ++i)
            {
                tmp2 = FooD(i, n - i);

            }
            End = DateTime.Now;

            Console.WriteLine("Funkcja dynamic: {0}", End - Start);

        }

        static int Foo(int x, int y)
        {
            return x * y + x + y - (x / y);
        }

        static dynamic FooD(dynamic x, dynamic y)
        {
            return x * y + x + y - (x / y);
        }
    }
}
