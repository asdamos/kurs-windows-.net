﻿namespace Zad3._1._1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.checkBoxDzienne = new System.Windows.Forms.CheckBox();
            this.checkBoxUzup = new System.Windows.Forms.CheckBox();
            this.name = new System.Windows.Forms.TextBox();
            this.adress = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.adress);
            this.groupBox1.Controls.Add(this.name);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(432, 80);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Uczelnia";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Adres:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nazwa:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.checkBoxUzup);
            this.groupBox2.Controls.Add(this.checkBoxDzienne);
            this.groupBox2.Controls.Add(this.comboBox1);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Location = new System.Drawing.Point(12, 98);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(432, 76);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Rodzaj studiów";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Cykl nauki:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 180);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Akceptuj";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(369, 180);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 3;
            this.button2.Text = "Anuluj";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.DisplayMember = "(none)";
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "3-letnie",
            "5-letnie"});
            this.comboBox1.Location = new System.Drawing.Point(73, 20);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(353, 21);
            this.comboBox1.TabIndex = 1;
            // 
            // checkBoxDzienne
            // 
            this.checkBoxDzienne.AutoSize = true;
            this.checkBoxDzienne.Location = new System.Drawing.Point(73, 48);
            this.checkBoxDzienne.Name = "checkBoxDzienne";
            this.checkBoxDzienne.Size = new System.Drawing.Size(65, 17);
            this.checkBoxDzienne.TabIndex = 2;
            this.checkBoxDzienne.Text = "Dzienne";
            this.checkBoxDzienne.UseVisualStyleBackColor = true;
            this.checkBoxDzienne.CheckedChanged += new System.EventHandler(this.checkBoxDzienne_CheckedChanged);
            // 
            // checkBoxUzup
            // 
            this.checkBoxUzup.AutoSize = true;
            this.checkBoxUzup.Location = new System.Drawing.Point(144, 48);
            this.checkBoxUzup.Name = "checkBoxUzup";
            this.checkBoxUzup.Size = new System.Drawing.Size(95, 17);
            this.checkBoxUzup.TabIndex = 3;
            this.checkBoxUzup.Text = "Uzupełniające";
            this.checkBoxUzup.UseVisualStyleBackColor = true;
            this.checkBoxUzup.CheckedChanged += new System.EventHandler(this.checkBoxUzup_CheckedChanged);
            // 
            // name
            // 
            this.name.Location = new System.Drawing.Point(57, 20);
            this.name.Name = "name";
            this.name.Size = new System.Drawing.Size(369, 20);
            this.name.TabIndex = 2;
            this.name.Text = "Uniwersytet Wrocławski";
            // 
            // adress
            // 
            this.adress.Location = new System.Drawing.Point(57, 46);
            this.adress.Name = "adress";
            this.adress.Size = new System.Drawing.Size(369, 20);
            this.adress.TabIndex = 3;
            this.adress.Text = "pl. Uniwersytecki 1, 50-137 Wrocław";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(456, 211);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TextBox adress;
        private System.Windows.Forms.TextBox name;
        private System.Windows.Forms.CheckBox checkBoxUzup;
        private System.Windows.Forms.CheckBox checkBoxDzienne;
    }
}

