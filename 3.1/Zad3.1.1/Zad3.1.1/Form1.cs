﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Zad3._1._1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            this.Text = "Wybór Uczelni";
            this.comboBox1.SelectedIndex = 0;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            String s = this.name.Text + "\n";
            s += this.adress.Text + "\n";
            s += this.comboBox1.Text + "\n";
            if (checkBoxDzienne.Checked)
            {
                s += "Dzienne";
            }
            else if (checkBoxUzup.Checked)
            {
                s += "Uzupełniające";
            }
            MessageBox.Show(s,  "Uczelnia");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void checkBoxDzienne_CheckedChanged(object sender, EventArgs e)
        {
           if(checkBoxDzienne.Checked)
           {
                checkBoxUzup.Checked = false;
           }
        }

        private void checkBoxUzup_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxUzup.Checked)
            {
                checkBoxDzienne.Checked = false;
            }
        }
    }
}
