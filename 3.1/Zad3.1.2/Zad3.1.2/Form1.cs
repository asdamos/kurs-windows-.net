﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace Zad3._1._2
{
    public partial class Form1 : Form
    {
        private void Calculate(int i)
        {
            double pow = i* i - i * (i - i / 5);
            //MessageBox.Show(pow.ToString(), "My Application");

        }

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            TreeNode[] array1 = new TreeNode[] { new TreeNode("Windows 10"), new TreeNode("Windows 7") };

            TreeNode[] array2 = new TreeNode[] { new TreeNode("Debian"), new TreeNode("Suse"), new TreeNode("Ubuntu") };

            TreeNode[] array3 = new TreeNode[] { new TreeNode("OS X 10.11"), new TreeNode("OS X 10.9"), new TreeNode("OS X 10.8") };

            TreeNode[] array4 = new TreeNode[] { new TreeNode("2.3 Gingerbread"),
                new TreeNode("4.4 KitKat"), new TreeNode("5.0 Lollipop"), new TreeNode("6.0 Marshmallow"), new TreeNode("7.1 Nougat")  };

            TreeNode nodeWindows = new TreeNode("Windows", array1);
            TreeNode nodeLinux = new TreeNode("Linux", array2);
            TreeNode nodeMacOS = new TreeNode("Mac OS",array3);
            TreeNode nodeAndroid = new TreeNode("Android", array4);

            treeView1.Nodes.Add(nodeWindows);
            treeView1.Nodes.Add(nodeLinux);
            treeView1.Nodes.Add(nodeMacOS);
            treeView1.Nodes.Add(nodeAndroid);

            progressBar1.Maximum = 100;
            progressBar1.Step = 1;
            progressBar1.Value = 0;

            toolStripProgressBar1.Maximum = 100;
            toolStripProgressBar1.Step = 1;
            toolStripProgressBar1.Value = 0;


        }

        private void toolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = "Button 1 clicked";

            if (progressBar1.Value + 5 <= progressBar1.Maximum) progressBar1.Value += 5;

            if (toolStripProgressBar1.Value + 5 <= toolStripProgressBar1.Maximum) toolStripProgressBar1.Value += 5;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = "Button 2 clicked";

            if (progressBar1.Value + 10 <= progressBar1.Maximum) progressBar1.Value += 10;

            if (toolStripProgressBar1.Value + 10 <= toolStripProgressBar1.Maximum) toolStripProgressBar1.Value += 10;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = "Button 3 clicked";

            if (progressBar1.Value + 20 <= progressBar1.Maximum) progressBar1.Value += 20;

            if (toolStripProgressBar1.Value + 20 <= toolStripProgressBar1.Maximum) toolStripProgressBar1.Value += 20;
        }

        private void button0_Click(object sender, EventArgs e)
        {
            progressBar1.Value = 0;
            toolStripProgressBar1.Value = 0;
        }
    }
}
