﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace Zad3._1._4
{
    class SmoothProgressBar : UserControl
    {
        int min = 0;
        int max = 100;
        int val = 0;
        Color BarColor = Color.Blue;// Color of progress meter

        protected override void OnResize(EventArgs e)
        {
            // Invalidate the control to get a repaint.
            this.Invalidate();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            Graphics g = e.Graphics;

            

            float percent = (float)(val - min) / (float)(max - min);
            Rectangle rect = this.ClientRectangle;

            LinearGradientBrush linGrBrush = new LinearGradientBrush(
            new Point(rect.Left, 0),
            new Point(rect.Right, 0),
            Color.White,
            BarColor);

            // Calculate area for drawing the progress.
            rect.Width = (int)((float)rect.Width * percent);

            // Draw the progress meter.
            g.FillRectangle(linGrBrush, rect);

            // Draw a three-dimensional border around the control.
            Draw3DBorder(g);

            // Clean up.
            linGrBrush.Dispose();
            g.Dispose();
        }

        public int Min
        {
            get
            {
                return min;
            }

            set
            {
                if (value < 0)
                {
                    min = 0;
                }
                if (value > max)
                {
                    min = value;
                    min = value;
                }
                if (val < min)
                {
                    val = min;
                }
                this.Invalidate();
            }
        }

        public int Max
        {
            get
            {
                return max;
            }

            set
            {
                if (value < min)
                {
                    min = value;
                }

                max = value;

                if (val > max)
                {
                    val = max;
                }
                this.Invalidate();
            }
        }

        public int Value
        {
            get
            {
                return val;
            }

            set
            {
                int oldValue = val;

                if (value < min)
                {
                    val = min;
                }
                else if (value > max)
                {
                    val = max;
                }
                else
                {
                    val = value;
                }

                this.Invalidate();
            }
        }

        public Color ProgressBarColor
        {
            get
            {
                return BarColor;
            }

            set
            {
                BarColor = value;
                this.Invalidate();
            }
        }

        private void Draw3DBorder(Graphics g)
        {
            int PenWidth = 1;

            g.DrawLine(Pens.DarkGray,
            new Point(this.ClientRectangle.Left, this.ClientRectangle.Top),
            new Point(this.ClientRectangle.Width - PenWidth, this.ClientRectangle.Top));

            g.DrawLine(Pens.DarkGray,
            new Point(this.ClientRectangle.Left, this.ClientRectangle.Top),
            new Point(this.ClientRectangle.Left, this.ClientRectangle.Height - PenWidth));

            g.DrawLine(Pens.White,
            new Point(this.ClientRectangle.Left, this.ClientRectangle.Height - PenWidth),
            new Point(this.ClientRectangle.Width - PenWidth, this.ClientRectangle.Height - PenWidth));

            g.DrawLine(Pens.White,
            new Point(this.ClientRectangle.Width - PenWidth, this.ClientRectangle.Top),
            new Point(this.ClientRectangle.Width - PenWidth, this.ClientRectangle.Height - PenWidth));
        }

    }
}
