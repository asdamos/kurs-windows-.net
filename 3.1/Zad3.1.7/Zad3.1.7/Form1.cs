﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Net;
using System.Net.Http;

namespace Zad3._1._7
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            WebClient client = new WebClient();
            string reply = client.DownloadString("http://www.google.pl");
            textBox1.Text = reply;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Task t = new Task(DownloadPageAsync);
            t.Start();
        }


        async void DownloadPageAsync()
        {
            string page = "http://www.google.pl";
            using (HttpClient client = new HttpClient())
            using (HttpResponseMessage response = await client.GetAsync(page))
            using (HttpContent content = response.Content)
            {

                string result = await content.ReadAsStringAsync();

                textBox2.Text = result;
            }
        }
    }
}
