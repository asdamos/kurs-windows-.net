﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad3._3._1
{
    class Program
    {
        static void Main(string[] args)
        {
            Complex z = new Complex(4, 3);
            Console.WriteLine(String.Format("{0}", z));
            Console.WriteLine(String.Format("{0:d}", z));
            Console.WriteLine(String.Format("{0:w}", z));
        }
    }

    class Complex : IFormattable
    {
        private int real;
        private int imaginary;

        public Complex(int real, int imaginary)
        {
            this.real = real;
            this.imaginary = imaginary;
        }

        public string ToString(string format, IFormatProvider formatProvider)
        {
            if (String.IsNullOrEmpty(format))
            {
                format = "d";
            }

            switch (format)
            {
                case "d":
                    return (real.ToString() +  " + " + imaginary.ToString() + "i");
                case "w":
                    return ("[" + real.ToString() + "," + imaginary.ToString() + "]");
                default:
                    return (real.ToString() + " + " + imaginary.ToString() + "i");

            }
        }

        public static Complex operator +(Complex c1, Complex c2)
        {
            return new Complex(c1.real + c2.real, c1.imaginary + c2.imaginary);
        }

        public static Complex operator -(Complex c1, Complex c2)
        {
            return new Complex(c1.real - c2.real, c1.imaginary - c2.imaginary);
        }

        public static Complex operator *(Complex c1, Complex c2)
        {
            int a = c1.real;
            int b = c1.imaginary;
            int c = c2.real;
            int d = c2.imaginary;

            return new Complex(a*c - b*d, b*c + a*d);
        }

        public static Complex operator /(Complex c1, Complex c2)
        {
            int a = c1.real;
            int b = c1.imaginary;
            int c = c2.real;
            int d = c2.imaginary;

            return new Complex((a*c + b*d)/(c*c + d*d), (b*c - a*d)/(c*c + d*d));
        }

    }
}
