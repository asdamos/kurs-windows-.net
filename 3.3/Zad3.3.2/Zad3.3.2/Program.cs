﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad3._3._2
{
    class Program
    {
        static void Main(string[] args)
        {
            IntSet set = new IntSet();

            set.Add(1);
            set.Add(10);
            set.Add(1);
            
            Console.WriteLine(set.ToString());

            set.Add(11);
            set.Remove(10);

            Console.WriteLine(set.ToString());
        }
    }

    class IntSet
    {
        private SortedSet<int> set;

        public IntSet()
        {
            set = new SortedSet<int>();
        }


        public bool Add(int e)
        {
            return set.Add(e);
        }

        public bool Contains(int e)
        {
            return set.Contains(e);
        }

        public bool Remove(int e)
        {
            return set.Remove(e);
        }

        public void Clear()
        {
            set.Clear();
        }

        public string ToString()
        {
            string res = "";
            foreach(var e in set)
            {
                res += e.ToString() + " ";
            }
            return res;
        }

    }
}
