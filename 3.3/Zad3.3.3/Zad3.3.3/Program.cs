﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.IO.Compression;

namespace Zad3._3._3
{
    class Program
    {
        static void Main(string[] args)
        {
            Aes aesAlg = Aes.Create();
            ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

            ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

            CompressThenEncrypt("plik.txt", "zaszyfrowany.gzip", encryptor);
            DecryptThenDecompress("zaszyfrowany.gzip", "odszyfrowany.txt", decryptor);
        }

        private static void CompressThenEncrypt(string inputFileName, string outputFileName, ICryptoTransform encryptor)
        {
            using (var inputFileStream = new FileStream(inputFileName, FileMode.Open, FileAccess.Read))
            {
                using (var outputFileStream = new FileStream(outputFileName, FileMode.Create, FileAccess.Write))
                using (var cryptoStream = new CryptoStream(outputFileStream, encryptor, CryptoStreamMode.Write))
                using (var gZipStream = new GZipStream(cryptoStream, CompressionMode.Compress))
                {
                    inputFileStream.CopyTo(gZipStream);
                }
            }
        }

        private static void DecryptThenDecompress(string inputFileName, string outputFileName, ICryptoTransform decryptor)
        {
            using (var inputFileStream = new FileStream(inputFileName, FileMode.Open, FileAccess.Read))
            {
                using (var cryptoStream = new CryptoStream(inputFileStream, decryptor, CryptoStreamMode.Read))
                using (var gZipStream = new GZipStream(cryptoStream, CompressionMode.Decompress))
                using (var outputFileStream = new FileStream(outputFileName, FileMode.Create, FileAccess.Write))
                {
                    gZipStream.CopyTo(outputFileStream);
                }
            }
        }
    }
}
