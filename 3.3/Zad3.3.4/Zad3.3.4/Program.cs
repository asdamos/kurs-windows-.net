﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Zad3._3._4
{
    class Program
    {
        private static Semaphore barberReady = new Semaphore(0, 1);
        private static Semaphore accessWRSeats = new Semaphore(1, 1);
        private static Semaphore custReady = new Semaphore(0, 9);

        private static int numberOfFreeWRSeats = 9;

        static void Main(string[] args)
        {
            Thread barber = new Thread(new ThreadStart(Barber));
            barber.Start();

            for(int i=0; i<100; i++)
            {
                Thread t = new Thread(new ParameterizedThreadStart(Customer));
                t.Start(i);
            }

        }

        private static void Barber()
        {
            while(true)
            {
                custReady.WaitOne();
                accessWRSeats.WaitOne();
                numberOfFreeWRSeats += 1;
                barberReady.Release();
                accessWRSeats.Release();

                Console.WriteLine("Barber cuts hair");
                Thread.Sleep(500);
            }
        }

        private static void Customer(object num)
        {
            accessWRSeats.WaitOne();
            if (numberOfFreeWRSeats > 0)
            {
                numberOfFreeWRSeats -= 1;
                custReady.Release();

                accessWRSeats.Release();
                barberReady.WaitOne();
                Console.WriteLine("Customer {0} gets haircut", num);
            }
            else
            {
                accessWRSeats.Release();
            }
        }
    }
}
