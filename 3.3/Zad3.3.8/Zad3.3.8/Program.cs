﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad3._3._8
{
    class Program
    {
        static void Main(string[] args)
        {
            CultureInfo[] cultures = new CultureInfo[7];
            cultures[0] = new CultureInfo("en-US");
            cultures[1] = new CultureInfo("de-DE");
            cultures[2] = new CultureInfo("fr-FR");
            cultures[3] = new CultureInfo("ru-RU");
            cultures[4] = new CultureInfo("ar");
            cultures[5] = new CultureInfo("cs-CZ");
            cultures[6] = new CultureInfo("pl-PL");

            for(int i=0; i<7; i++)
            {
                Console.WriteLine("\n\nCulture name: {0} -> {1}", cultures[i].Name, cultures[i].NativeName);

                Console.WriteLine("Months:");

                DateTimeFormatInfo info = DateTimeFormatInfo.GetInstance(cultures[i]);

                for (int j=1; j<13; j++)
                {
                    Console.WriteLine("{0} - {1}", info.GetMonthName(j), info.GetAbbreviatedMonthName(j));
                }

                Console.WriteLine("\nDays of week");

                DayOfWeek[] days = new DayOfWeek[7] { DayOfWeek.Monday, DayOfWeek.Tuesday, DayOfWeek.Wednesday, DayOfWeek.Thursday, DayOfWeek.Friday, DayOfWeek.Saturday, DayOfWeek.Sunday };

                for (int j = 0; j < 7; j++)
                {
                    Console.WriteLine("{0} - {1}", info.GetDayName(days[j]), info.GetAbbreviatedDayName(days[j]));
                }
                DateTime dt = DateTime.Now;
                Console.WriteLine("\nToday date");
                Console.WriteLine("{0}", dt.ToString(cultures[i]));

            }



        }
    }
}
